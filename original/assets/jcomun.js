let lgAT;
let lgDC;
let lgTDS;
let lgT;
let varVolumen;
let fila = [];

// eslint-disable-next-line no-unused-vars
function NacenTobogs() {
  let a = $('#sliderLSI').slider();
  a.slider({
    min: -2
  });
  a.slider({
    max: 2
  });
  a.slider({
    step: 0.01
  });
  a.slider('value', 0);
  $('#sliderLSI .ui-slider-handle').unbind('keydown');
  $('#sliderLSI').unbind();
  a = $('#sliderPH').slider();
  a.slider({
    min: 6.5
  });
  a.slider({
    max: 8.5
  });
  a.slider({
    step: 0.1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 7.2);
  a = $('#sliderAT').slider();
  a.slider({
    min: 0
  });
  a.slider({
    max: 300
  });
  a.slider({
    step: 1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 100);
  $('#entr_FinalAT').val(100);
  a = $('#sliderDC').slider();
  a.slider({
    min: 0
  });
  a.slider({
    max: 700
  });
  a.slider({
    step: 1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 300);
  $('#entr_FinalDC').val(300);
  a = $('#sliderCL').slider();
  a.slider({
    min: 0
  });
  a.slider({
    max: 5
  });
  a.slider({
    step: 0.1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 1.5);
  $('#entr_FinalCL').val(1.5);
  $('#escala_CL').val('0 - 4 ppm ');
  a = $('#sliderBR').slider();
  a.slider({
    min: 0
  });
  a.slider({
    max: 7
  });
  a.slider({
    step: 0.1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 3);
  $('#entr_FinalBR').val(3);
  $('#escala_BR').val('0 - 7 ppm ');
  a = $('#sliderCF').slider();
  a.slider({
    min: 1
  });
  a.slider({
    max: 10
  });
  a.slider({
    step: 0.1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 4);
  $('#entr_FinalCF').val(4);
  $('#entr_FF').val(2.5);
  a = $('#sliderVF').slider();
  a.slider({
    min: 25
  });
  a.slider({
    max: 65
  });
  a.slider({
    step: 1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 40);
  $('#entr_FinalVF').val(40);
  $('#entr_DF').val(282);
  a = $('#sliderVFB').slider();
  a.slider({
    min: 1
  });
  a.slider({
    max: 6
  });
  a.slider({
    step: 0.1
  });
  a.slider({
    animate: true
  });
  a.slider('value', 4);
  $('#entr_FinalVFB').val(4);
  $('#entr_DFB').val(0.63);
  $('input:disabled').val(0);
  document.getElementById('entr_volumen').value = 1E4;
  addOptionT('entr_temp');
  addOptionACN('entr_cianurico');
  document.getElementById('entr_cianurico').selectedIndex = 0;
  document.getElementById('entr_tds').value = 0;
  addOptionANOS('entr_tds_anos');
  document.getElementById('entr_tds_anos').selectedIndex = 2;
  document.getElementById('entr_tds_estimado').value = 1210;
  addOptionPH();
  document.getElementById('check_pc').checked = true;
  document.getElementById('check_be').checked = false;
  addOptionPCSpc('perc_salfuman');
  document.getElementById('entr_InicioAT').value = 100;
  document.getElementById('alc_carb').value = 100;
  document.getElementById('entr_InicioDC').value = 300;
  document.getElementById('dur_calcica_agua_remplazo').value = 100;
  document.getElementById('escala_CL').selectedIndex = 0;
  document.getElementById('escala_BR').selectedIndex = 0;
  document.getElementById('entr_InicioCL').value = 1.5;
  document.getElementById('entr_InicioBR').value = 3;
  addOptionPCL();
  document.getElementById('entr_FinalCL').value = 1.5;
  document.getElementById('CLactivado').value = 1;
  calcularLSI();
}

function actFFporCF(a) {
  varVolumen = $('#entr_volumen').val() / 1E3;
  a = redondearNumOpt(varVolumen / a);
  $('#entr_FF').val(a);
  diaFiltro();
  areaFiltro();
}

function actCFporFF(a) {
  varVolumen = $('#entr_volumen').val() / 1E3;
  a = redondearNum(varVolumen / a, 1);
  diaFiltro();
  return a;
}

function actDFporVF() {
  diaFiltro();
}

function actDFBporVFB() {
  areaFiltro();
}

function actVFporDF() {
  const a = document.getElementById('entr_FF').value;
  const c = document.getElementById('entr_DF').value;
  return redondearNum(4E6 * a / (Math.PI * c * c), 0);
}

function actVFBporDFB() {
  const a = redondearNumOpt(document.getElementById('entr_FF').value / document.getElementById('entr_DFB').value);
  $('#sliderVFB').slider('value', a);
}

function diaFiltro() {
  let a = document.getElementById('entr_FF').value;
  a = a / document.getElementById('entr_FinalVF').value;
  a = a / Math.PI;
  a = redondearNum(2E3 * Math.sqrt(a), 0);
  document.getElementById('entr_DF').value = redondearNum(a, 0);
}

function areaFiltro() {
  let s = document.getElementById('entr_FF').value;
  s /= document.getElementById('entr_FinalVFB').value;
  document.getElementById('entr_DFB').value = redondearNum(s, 2);
}

function actualizarAgua() {
  calcularPH();
  calcularAT();
  calcularDC();
  calcularCL();
  calcularBR();
  calcularDepuradoraDefecto();
}

function calcularDepuradoraDefecto() {
  $('#sliderCF').slider('value', 4);
  $('#sliderVF').slider('value', 40);
  $('#sliderVFB').slider('value', 4);
}

function calcularPH() {
  calcularLSI();
  varVolumen = document.getElementById('entr_volumen').value / 1E3;
  let a = document.getElementById('entr_FinalPH').value;
  let c = document.getElementById('entr_InicioPH').value;
  c = redondearNum(a - c, 2);
  const b = document.getElementById('perc_salfuman').value;
  const d = document.getElementById('check_pc').checked;
  const e = redondearNum(10 * (8.5 - a), 0);

  a = fila[e] * varVolumen;
  if (c <= -0.05) {
    $('#minPH').addClass('color2');
    $('#incPH').removeClass('color2');
    document.getElementById('entr_carb_sod').value = 0;
    if (d === true) {
      c = 30 * a / b;
      document.getElementById('entr_salfuman').value = redondearNumOpt(c);
      document.getElementById('entr_bisulf').value = redondearNumOpt(1.26 * a);
    } else {
      c = 30 * a / (1.52 * b);
      document.getElementById('entr_salfuman').value = redondearNumOpt(c);
      document.getElementById('entr_bisulf').value = redondearNumOpt(1.26 * c);
    }
  } else if (c >= 0.05) {
    $('#incPH').addClass('color2');
    $('#minPH').removeClass('color2');
    document.getElementById('entr_carb_sod').value = redondearNumOpt(1.055 * a);
  } else {
    $('#minPH').removeClass('color2');
    $('#incPH').removeClass('color2');
    document.getElementById('entr_carb_sod').value = 0;
    document.getElementById('entr_salfuman').value = 0;
    document.getElementById('entr_bisulf').value = 0;
  }
}

// eslint-disable-next-line no-unused-vars
function calcularT() {
  calcularLSI();
}

function calcularTDS() {
  let a = parseInt($('#entr_tds').val());
  const c = parseInt($('#entr_tds_anos').val());
  const b = parseInt($('#entr_FinalDC').val());
  const d = parseInt($('#alc_carb').val());

  if (a === 0) {
    a = redondearNum(25 + (b + d) * (1.6 + 0.67 * c), 0);
  }

  $('#entr_tds_estimado').val(a);
}

function calcularAT(a) {
  calcularLSI();
  varVolumen = $('#entr_volumen').val();
  const c = $('#sliderAT').slider('value');
  const b = $('#entr_InicioAT').val();

  if (c > b) {
    a = redondearNumOpt(0.00174 * (c - b) * varVolumen);
    $('#entr_bicarb').val(a);
    $('#incAT').addClass('color2');
    $('#minAT').removeClass('color2');
  } else if (c < b) {
    $('#entr_bicarb').val(0);
    if (a === 'iis') {
      $('#sliderAT').slider('value', b);
    } else {
      $('#sliderAT').slider('value', b);
      $('#minAT').addClass('color2');
      $('#incAT').removeClass('color2');
    }
  } else {
    $('#minAT').removeClass('color2');
    $('#incAT').removeClass('color2');
    $('#entr_bicarb').val(0);
  }
}

function calcularACc() {
  let a = $('#sliderAT').slider('value');
  const c = $('#entr_cianurico').val();
  const b = $('#entr_FinalPH').val();
  let d = redondearNum(0.12 + 0.2 * (b - 6.5), 2);

  if (b >= 7.55) {
    d = redondearNum(0.32 + 0.1 * (b - 7.5), 2);
  }

  if (b >= 7.85) {
    d = redondearNum(0.35 + 0.05 * (b - 7.8), 3);
  }

  a = redondearNumOpt(a - d * c);
  $('#alc_carb').val(a);
}

function calcularDC(a) {
  calcularLSI();
  varVolumen = $('#entr_volumen').val();
  const c = $('#sliderDC').slider('value');
  const b = $('#dur_calcica_agua_remplazo').val();
  const d = $('#entr_InicioDC').val();
  const e = redondearNum((c - d) / (b - d) * varVolumen, 0);

  if (c > d) {
    document.getElementById('avisoDC').style.display = 'none';
    $('#remplazo_agua').val(0);
    a = redondearNumOpt(0.00111 * (c - d) * varVolumen);
    $('#entr_clor_cal').val(a);
    a = redondearNumOpt(0.00147 * (c - d) * varVolumen);
    $('#entr_clor_cal_dihdr').val(a);
    $('#incDC').addClass('color2');
    $('#minDC').removeClass('color2');
    $('#avisoDC').hide();
  } else if (c < d) {
    $('#minDC').addClass('color2');
    $('#incDC').removeClass('color2');
    $('#entr_clor_cal').val(0);
    $('#entr_clor_cal_dihdr').val(0);

    if (a == 'iis') {
      $('#sliderDC').slider('value', d);
    } else {
      if (b > c || e > varVolumen) {
        if (b < c) {
          $('#sliderDC').slider('value', c);
        } else {
          $('#sliderDC').slider('value', b);
          document.getElementById('avisoDC').style.display = 'block';
          if (a === 'sc') {
            setTimeout(() => {
              $('#avisoDC').hide();
            }, 5E3);
          }
        }
      } else {
        $('#remplazo_agua').val(e / 1E3);
        $('#avisoDC').hide();
      }
    }
  } else {
    $('#incDC').removeClass('color2');
    $('#minDC').removeClass('color2');
    $('#entr_clor_cal').val(0);
    $('#entr_clor_cal_dihdr').val(0);
    $('#remplazo_agua').val(0);
  }
}

function CambioDCAgRemp() {
  varVolumen = $('#entr_volumen').val();
  const a = $('#sliderDC').slider('value');
  const c = +$('#dur_calcica_agua_remplazo').val();
  const b = +$('#entr_InicioDC').val();
  const d = redondearNum((a - b) / (c - b) * varVolumen, 0);

  if (a < b) {
    if (a >= c) {
      $('#remplazo_agua').val(d);
      $('#avisoDC').hide();
      '#sliderDC'.slider('value', a);
    } else {
      $('#sliderDC').slider('value', c);
      document.getElementById('avisoDC').style.display = 'block';
      setTimeout(() => {
        $('#avisoDC').hide();
      }, 5E3);
    }
  }
}

function calcularCL() {
  varVolumen = document.getElementById('entr_volumen').value;
  let a = document.getElementById('entr_InicioCL').value;
  let c = document.getElementById('entr_FinalCL').value;
  let b = parseInt(document.getElementById('perc_lejia').value);
  const d = c - a;
  a = (c - a) * varVolumen;
  c = document.getElementById('entr_cianurico').value;
  const e = parseInt(document.getElementById('perc_peroxido').value);
  if (a >= 0.05) {
    b = 0.1 * (a / b);
    document.getElementById('entr_lejia').value = redondearNumOpt(b);
    b = a / 650;
    document.getElementById('entr_hipocal').value = redondearNumOpt(b);
    b = a / 560;
    document.getElementById('entr_dicloro').value = redondearNumOpt(b);
    b = a / 900;
    document.getElementById('entr_tricloro').value = redondearNumOpt(b);
    b = 0.6 * d + 1 * c;
    document.getElementById('entr_ACN_Tri').value = redondearNumOpt(b);
    b = 0.9 * d + 1 * c;
    document.getElementById('entr_ACN_Di').value = redondearNumOpt(b);
    document.getElementById('entr_tiosulfito').value = 0;
    document.getElementById('entr_bisulfito').value = 0;
    document.getElementById('entr_metabisulfito').value = 0;
    document.getElementById('entr_peroxido').value = 0;
    document.getElementById('entr_vitC').value = 0;
    $('#incCL1').addClass('color2');
    $('#incCL2').addClass('color2');
    $('#incCL3').addClass('color2');
    $('#incCL4').addClass('color2');
    $('#minCL').removeClass('color2');
  } else if (a <= -0.05) {
    $('#incCL1').removeClass('color2');
    $('#incCL2').removeClass('color2');
    $('#incCL3').removeClass('color2');
    $('#minCL').addClass('color2');
    document.getElementById('entr_lejia').value = 0;
    document.getElementById('entr_hipocal').value = 0;
    document.getElementById('entr_dicloro').value = 0;
    document.getElementById('entr_tricloro').value = 0;
    document.getElementById('entr_ACN_Tri').value = c;
    document.getElementById('entr_ACN_Di').value = c;
    b = -(0.00186 * a);
    document.getElementById('entr_tiosulfito').value = redondearNumOpt(b);
    b = -(0.00161 * a);
    document.getElementById('entr_bisulfito').value = redondearNumOpt(b);
    b = -(0.00147 * a);
    document.getElementById('entr_metabisulfito').value = redondearNumOpt(b);
    b = -(0.00248 * a);
    document.getElementById('entr_vitC').value = redondearNumOpt(b);
    b = -(0.001 * a / (0.02 * e));
    document.getElementById('entr_peroxido').value = redondearNumOpt(b);
    document.getElementById('entr_ACN_Tri').value = redondearNumOpt(1 * c);
    document.getElementById('entr_ACN_Di').value = redondearNumOpt(1 * c);
  } else {
    $('#incCL1').removeClass('color2');
    $('#incCL2').removeClass('color2');
    $('#incCL3').removeClass('color2');
    $('#minCL').removeClass('color2'); document.getElementById('entr_ACN_Tri').value = c;
    document.getElementById('entr_ACN_Di').value = c;
    document.getElementById('entr_lejia').value = 0;
    document.getElementById('entr_hipocal').value = 0;
    document.getElementById('entr_dicloro').value = 0;
    document.getElementById('entr_tricloro').value = 0;
    document.getElementById('entr_tiosulfito').value = 0;
    document.getElementById('entr_bisulfito').value = 0;
    document.getElementById('entr_metabisulfito').value = 0;
    document.getElementById('entr_peroxido').value = 0;
    document.getElementById('entr_vitC').value = 0;
  }
}

function calcularBR() {
  varVolumen = document.getElementById('entr_volumen').value;
  let a = document.getElementById('entr_InicioBR').value;
  let c = document.getElementById('entr_FinalBR').value;
  const b = document.getElementById('perc_lejia_BR').value;
  a = (c - a) * varVolumen;

  if (a >= 0.5) {
    c = a / 1260;
    document.getElementById('entr_bromocloro').value = redondearNumOpt(c);
    c = a / 1110;
    document.getElementById('entr_dibromo').value = redondearNumOpt(c);
    c = 0.1 * (a / b) / 2.25;
    document.getElementById('entr_lejia_BR').value = redondearNumOpt(c);
    c = a / 650 / 2.25;
    document.getElementById('entr_hipocal_BR').value = redondearNumOpt(c);
    c = a / 234 / 2.25;
    document.getElementById('entr_MPS').value = redondearNumOpt(c);
    document.getElementById('entr_tiosulfitoBR').value = 0;
    $('#incBR1').addClass('color2');
    $('#incBR2').addClass('color2');
    $('#minBR').removeClass('color2');
  } else if (a <= -0.5) {
    $('#incBR1').removeClass('color2');
    $('#incBR2').removeClass('color2');
    $('#minBR').addClass('color2');
    document.getElementById('entr_lejia_BR').value = 0;
    document.getElementById('entr_bromocloro').value = 0;
    document.getElementById('entr_dibromo').value = 0;
    document.getElementById('entr_hipocal_BR').value = 0;
  } else {
    $('#incBR1').removeClass('color2');
    $('#incBR2').removeClass('color2');
    $('#minBR').removeClass('color2');
    document.getElementById('entr_lejia_BR').value = 0;
    document.getElementById('entr_bromocloro').value = 0;
    document.getElementById('entr_dibromo').value = 0;
    document.getElementById('entr_hipocal_BR').value = 0;
    document.getElementById('entr_tiosulfitoBR').value = 0;
  }
}

function calcularLSI() {
  let a = parseInt(document.getElementById('entr_tds_estimado').value);
  a = NoMenorDeUno(a);
  a = Math.log(a) / Math.log(10);
  lgTDS = 11.13 + 0.333 * a;
  lgTDS = redondearNum(lgTDS, 3);
  document.getElementById('tedeese').value = lgTDS;
  a = parseInt(document.getElementById('entr_temp').value);
  a = 1.8 * a + 32;
  a = -5E-7 * Math.pow(a, 3) + 6E-5 * Math.pow(a, 2) + 0.0117 * a - 0.4116;
  lgT = redondearNum(a, 2);
  document.getElementById('temperatura').value = lgT;
  a = parseInt(document.getElementById('alc_carb').value);
  a = NoMenorDeUno(a);
  lgAT = Math.log(a) / Math.log(10);
  lgAT = redondearNum(lgAT, 2);
  document.getElementById('alcalinidad').value = lgAT;
  a = parseInt(document.getElementById('entr_FinalDC').value);
  a = NoMenorDeUno(a);
  a = Math.log(a) / Math.log(10);
  lgDC = a - 0.4;
  lgDC = redondearNum(lgDC, 2);
  document.getElementById('dureza').value = lgDC;
  const phFIN = parseFloat(document.getElementById('entr_FinalPH').value);
  a = phFIN + lgT + lgDC + lgAT - lgTDS;
  a = redondearNum(a, 2);
  $('#entr_LSI').val(a);
  $('#sliderLSI').slider('value', a);

  if (a > 0.3) {
    a = '<strong>Atenci\u00f3n: ISL alto</strong><p>Incrustaciones calc\u00e1reas</p>';
    $('#t-LSIen').html('<strong>Warning! LSI high</strong><p>Scale-forming water</p>');
    $('#t-LSIen').removeClass('color3').addClass('colorRojo');
    $('#t-LSIes').html(a);
    $('#t-LSIes').removeClass('color3').addClass('colorRojo');
  } else if (a <= -0.3) {
    a = '<strong>Atencin: ISL bajo</strong><p>Agua corrosiva</p>';
    $('#t-LSIen').html('<strong>Warning! LSI low</strong><p>Corrosive water</p>');
    $('#t-LSIen').removeClass('color3').addClass('colorRojo');
    $('#t-LSIes').html(a);
    $('#t-LSIes').removeClass('color3').addClass('colorRojo');
  } else {
    a = '<strong>ISL correcto</strong><p>Agua equilibrada</p>';
    $('#t-LSIen').html('<strong>LSI OK</strong><p>Balanced water</p>');
    $('#t-LSIen').removeClass('colorRojo').addClass('color3');
    $('#t-LSIes').html(a);
    $('#t-LSIes').removeClass('colorRojo').addClass('color3');
  }
}

function NoMenorDeUno(a) {
  return a > 1 ? a: 1;
}

function redondearNum(a, c) {
  return Math.round(a * Math.pow(10, c)) / Math.pow(10, c);
}

function redondearNumOpt(a) {
  let c = 0;

  if (a <= 10) {
    c = 1;
  }

  if (a <= 1) {
    c = 2;
  }

  return Math.round(a * Math.pow(10, c)) / Math.pow(10, c);
}

function addOptionPH() {
  const a = document.getElementById('entr_InicioPH');
  const c = document.getElementById('entr_FinalPH');
  const b = document.getElementById('entr_FinalPHaux');
  a.options.length = 0;
  c.options.length = 0;
  b.options.length = 0;

  for (let d = 650; d < 851; d++) {
    const e = d / 100;
    b.options.add(new Option(e, e));
    b.value = 7.2;
  }

  for (let d = 65; d < 86; d++) {
    const e = d / 10;
    redondearNum(d / 100, 1);
    a.options.add(new Option(e, e));
    a.value = 7.2;
    c.options.add(new Option(e, e));
    c.value = 7.2;
    obtenerFilaPH('7.2');
  }
}

function addOptionT(a) {
  a = document.getElementById(a);
  a.options.length = 0;
  for (let c = 5; c < 51; c++) a.options.add(new Option(c + ' C', c));
  a.value = 25;
}

function addOptionACN(a) {
  a = document.getElementById(a);
  for (let c = a.options.length = 0; c < 27; c++) {
    a.options.add(new Option(2 * c));
  }
  for (let c = 0; c < 11; c++) {
    a.options.add(new Option(56 + 4 * c));
  }
  for (let c = 0; c < 10; c++) {
    a.options.add(new Option(100 + 5 * c));
  }
  for (let c = 0; c < 6; c++) {
    a.options.add(new Option(150 + 10 * c));
  }
}

function addOptionANOS(a) {
  a = document.getElementById(a);
  for (let c = a.options.length = 0; c < 10; c++) a.options.add(new Option(c));
}

function addOptionPCSpc(a) {
  a = document.getElementById(a);
  a.options.length = 0;
  for (let c = 10; c < 41; c++) a.options.add(new Option(c + ' %', c));
  a.value = 20;
}

function addOptionPCSbe(a) {
  a = document.getElementById(a);
  a.options.length = 0;
  for (let c = 6; c < 28; c++) a.options.add(new Option(c + '\u00baBe', c));
  a.value = 13;
}

function addOptionPCL() {
  const a = document.getElementById('perc_lejia');
  const c = document.getElementById('perc_lejia_BR');
  a.options.length = 0;
  c.options.length = 0;
  for (let b = 1; b < 21; b++) {
    const d = b + '%';
    a.options.add(new Option(d, b));
    a.value = 5;
    c.options.add(new Option(d, b));
    c.value = 5;
  }
}

// eslint-disable-next-line no-unused-vars
function check11() {
  if (document.getElementById('check_pc').checked === true) {
    document.getElementById('check_be').checked = false;
    addOptionPCSpc('perc_salfuman');
  } else {
    document.getElementById('check_be').checked = true;
    addOptionPCSbe('perc_salfuman');
  }
  calcularPH();
}

// eslint-disable-next-line no-unused-vars
function check22() {
  if (document.getElementById('check_be').checked === true) {
    document.getElementById('check_pc').checked = false;
    addOptionPCSbe('perc_salfuman');
  } else {
    document.getElementById('check_pc').checked = true;
    addOptionPCSpc('perc_salfuman');
  }

  calcularPH();
}

// eslint-disable-next-line no-unused-vars
function numbersOnly(a) {
  const c = false;
  let b;
  if (window.event) {
    b = window.event.keyCode;
  } else if (a) {
    b = a.which;
  } else {
    return true;
  }

  const d = String.fromCharCode(b);
  const validCodes = [null, 0, 8, 9, 13, 27].includes(b);
  const validCharacters = '0123456789.'.includes(d);
  return validCodes || validCharacters || !!(c && d === '.');
}

function obtenerFilaPH(a) {
  switch (a) {
    case '8.5':
      fila = [0, 2.4, 4.5, 6.3, 8, 10, 11, 13, 14, 16, 18, 21, 23, 27, 30, 35, 40, 46, 53, 61, 71];
      break;
    case '8.4':
      fila = [2.4, 0, 2.1, 4, 5.7, 7.4, 9, 11, 13, 15, 17, 19, 22, 26, 30, 35, 41, 47, 55, 64, 73];
      break;
    case '8.3':
      fila = [4.5, 2.1, 0, 1.9, 3.7, 5.4, 7.1, 8.9, 11, 13, 16, 18, 22, 26, 30, 35, 41, 48, 56, 66, 76];
      break;
    case '8.2':
      fila = [6.3, 4, 1.9, 0, 1.8, 3.6, 5.4, 7.3, 9.3, 12, 14, 17, 21, 25, 30, 36, 42, 49, 58, 67, 78];
      break;
    case '8.1':
      fila = [8, 5.7, 3.7, 1.9, 0, 1.8, 3.6, 5.6, 7.8, 10, 13, 16, 20, 25, 30, 36, 42, 50, 59, 69, 80];
      break;
    case '8':
      fila = [10, 7.4, 5.4, 3.1, 1.8, 0, 1.9, 3.9, 6.1, 8.7, 12, 15, 19, 24, 29, 35, 42, 51, 60, 70, 82];
      break;
    case '7.9':
      fila = [11, 9, 7.1, 4.7, 3.6, 1.9, 0, 2, 4.3, 7, 10, 14, 18, 23, 28, 35, 42, 51, 60, 71, 83];
      break;
    case '7.8':
      fila = [13, 11, 8.9, 6.4, 5.6, 3.9, 2, 0, 2.3, 5, 8.2, 12, 16, 21, 27, 34, 41, 50, 60, 72, 84];
      break;
    case '7.7':
      fila = [14, 13, 11, 8.2, 7.8, 6.1, 4.3, 2.3, 0, 2.7, 5.9, 10, 14, 19, 25, 32, 40, 49, 60, 71, 84];
      break;
    case '7.6':
      fila = [16, 15, 13, 10, 10, 8.7, 7, 5, 2.7, 0, 3.2, 7.1, 12, 17, 23, 30, 38, 48, 58, 70, 84];
      break;
    case '7.5':
      fila = [18, 17, 16, 13, 13, 12, 10, 8.2, 5.9, 3.2, 0, 3.9, 8.4, 14, 20, 27, 36, 45, 56, 69, 80];
      break;
    case '7.4':
      fila = [21, 19, 18, 15, 16, 15, 14, 12, 10, 7.1, 3.9, 0, 4.6, 10, 16, 24, 32, 42, 53, 66, 80];
      break;
    case '7.3':
      fila = [23, 22, 22, 19, 20, 19, 18, 16, 14, 12, 8.4, 4.6, 0, 5.5, 12, 19, 28, 38, 49, 62, 77];
      break;
    case '7.2':
      fila = [27, 26, 26, 22, 25, 24, 23, 21, 19, 17, 14, 10, 5.5, 0, 6.5, 14, 23, 33, 44, 57, 72];
      break;
    case '7.1':
      fila = [30, 30, 30, 26, 30, 29, 28, 27, 25, 23, 20, 16, 12, 6.5, 0, 7.6, 16, 27, 38, 52, 66];
      break;
    case '7':
      fila = [35, 35, 35, 30, 36, 35, 35, 34, 32, 30, 27, 24, 19, 14, 7.6, 0, 8.9, 19, 31, 44, 59];
      break;
    case '6.9':
      fila = [40, 41, 41, 35, 42, 42, 42, 41, 40, 38, 36, 32, 28, 23, 16, 8.9, 0, 10, 22, 36, 51];
      break;
    case '6.8':
      fila = [46, 47, 48, 41, 50, 51, 51, 50, 49, 48, 45, 42, 38, 33, 27, 19, 10, 0, 12, 25, 41];
      break;
    case '6.7':
      fila = [53, 55, 56, 48, 59, 60, 60, 60, 60, 58, 56, 53, 49, 44, 38, 31, 22, 12, 0, 14, 29];
      break;
    case '6.6':
      fila = [61, 64, 66, 56, 69, 70, 71, 72, 71, 70, 69, 66, 62, 57, 52, 44, 36, 25, 14, 0, 15];
      break;
    case '6.5':
      fila = [71, 73, 76, 78, 80, 82, 83, 84, 84, 84, 80, 80, 77, 72, 66, 59, 51, 41, 29, 15, 0];
  }
}

// Start program
// NacenTobogs();

// Start when document it's ready

$(document).ready(function() {
  $('#lblm-ayudaPH').click(function() {
    $('#ayudaPH').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaPH').click(function() {
    $('#ayudaPH').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaPH').show();
    return false;
  });
  $('#lblm-ayudaAT').click(function() {
    $('#ayudaAT').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 300
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaAT').click(function() {
    $('#ayudaAT').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaAT').show();
    return false;
  });
  $('#lblm-ayudaDC').click(function() {
    $('#ayudaDC').slideDown(2E3, function() {
      $('html, body').animate({
        scrollTop: 1E3
      }, 'slow');
    });
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaDC').click(function() {
    $('#ayudaDC').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaDC').show();
    return false;
  });
  $('#lblm-ayudaCL').click(function() {
    $('#ayudaCL').slideDown(1E3, function() {
      $('html, body').animate({
        scrollTop: 450
      }, 'slow');
    });
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaCL').click(function() {
    $('#ayudaCL').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaCL').show();
    return false;
  });
  $('#lblm-ayudaBR').click(function() {
    $('#ayudaBR').slideDown(2E3, function() {
      $('html, body').animate({
        scrollTop: 1E3
      }, 'slow');
    });
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaBR').click(function() {
    $('#ayudaBR').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaBR').show();
    return false;
  });
  $('#lblm-ayudaCF').click(function() {
    $('#ayudaCF').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaCF').click(function() {
    $('#ayudaCF').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaCF').show();
    return false;
  });
  $('#lblm-ayudaVF').click(function() {
    $('#ayudaVF').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaVF').click(function() {
    $('#ayudaVF').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaVF').show();
    return false;
  });
  $('#lblm-ayudaTDS').click(function() {
    $('#ayudaTDS').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaTDS').click(function() {
    $('#ayudaTDS').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaTDS').show();
    return false;
  });
  $('#lblm-ayudaACN').click(function() {
    $('#ayudaACN').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaACN').click(function() {
    $('#ayudaACN').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaACN').show();
    return false;
  });
  $('#lblm-ayudaLSI').click(function() {
    $('#ayudaLSI').slideDown(2E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
    $(this).hide();
    return false;
  });
  $('#lblo-ayudaLSI').click(function() {
    $('#ayudaLSI').slideUp(2E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
    $('#lblm-ayudaLSI').show();
    return false;
  });
  $('#lblo-valAC').click(function() {
    $('#p_alc_carb').removeClass('visible');
    $('#p_alc_carb').addClass('no_visible');
    $('#lblm-valAC').show();
    $(this).hide();
    return false;
  });
  $('#lblm-valAC').click(function() {
    $('#p_alc_carb').removeClass('no_visible');
    $('#p_alc_carb').addClass('visible');
    $('#lblo-valAC').show();
    $(this).hide();
    return false;
  });
  $('.content').hide();
  $('#tab1').show();
  $('a.tab').click(function() {
    $('.active').removeClass('active');
    $(this).addClass('active');
    $('.content').hide();
    const b = $(this).attr('name');
    $('#' + b).show();
  });
  $('#lblm-LSIcomp').click(function() {
    $(this).addClass('ocultar');
    $('#lblo-LSIcomp').removeClass('ocultar');
    $('#areaLSIcomp-aux').slideDown(1E3);
    $('html, body').animate({
      scrollTop: 210
    }, 'slow');
  });
  $('#lblo-LSIcomp').click(function() {
    $(this).addClass('ocultar');
    $('#lblm-LSIcomp').removeClass('ocultar');
    $('#areaLSIcomp-aux').slideUp(1E3);
    $('html, body').animate({
      scrollTop: 150
    }, 'slow');
  });

  $('#entr_volumen').change(() => {
    actualizarAgua();
  });
  $('#entr_tds_anos').change(() => {
    calcularTDS();
    calcularLSI();
  });
  $('#entr_tds').change(() => {
    calcularTDS();
    calcularLSI();
  });
  $('#sliderCF').slider({
    slide: function() {
      const b = $('#sliderCF').slider('value');
      $('#entr_FinalCF').val(b);
      actFFporCF(b);
    }
  });
  $('#sliderCF').slider({
    change: function() {
      const b = $('#sliderCF').slider('value');
      $('#entr_FinalCF').val(b);
      actFFporCF(b);
    }
  });
  $('#entr_FinalCF').change(() => {
    const b = $('#entr_FinalCF').val();
    $('#sliderCF').slider('value', b);
  });
  $('#entr_FF').change(() => {
    let b = $('#entr_FF').val();
    b = actCFporFF(b);
    $('#sliderCF').slider('value', b);
  });
  $('#sliderVF').slider({
    slide: function() {
      const b = $('#sliderVF').slider('value');
      $('#entr_FinalVF').val(b);
      actDFporVF();
    }
  });
  $('#sliderVF').slider({
    change: function() {
      const b = $('#sliderVF').slider('value');
      $('#entr_FinalVF').val(b);
      actDFporVF();
    }
  });
  $('#entr_FinalVF').change(() => {
    const b = $('#entr_FinalVF').val();
    $('#sliderVF').slider('value', b);
  });
  $('#entr_DF').change(() => {
    const b = actVFporDF();
    $('#sliderVF').slider('value', b);
  });
  $('#sliderVFB').slider({
    slide: function() {
      const b = $('#sliderVFB').slider('value');
      $('#entr_FinalVFB').val(b);
      actDFBporVFB();
    }
  });
  $('#sliderVFB').slider({
    change: function() {
      const b = $('#sliderVFB').slider('value');
      $('#entr_FinalVFB').val(b);
      actDFBporVFB();
    }
  });
  $('#entr_FinalVFB').change(() => {
    const b = $('#entr_FinalVFB').val();
    $('#sliderVFB').slider('value', b);
  });
  $('#entr_DFB').change(() => {
    $('#entr_DFB').val();
    actVFBporDFB();
  });
  $('#sliderPH').slider({
    slide: function() {
      const b = $('#sliderPH').slider('value');
      $('#entr_FinalPH').val(b);
      calcularACc();
      calcularPH();
    }
  });
  $('#sliderPH').slider({
    change: function() {
      const b = $('#sliderPH').slider('value');
      $('#entr_FinalPH').val(b);
      calcularACc();
      calcularPH();
    }
  });
  $('#entr_FinalPH').change(() => {
    $('#sliderPH').slider('value', $('#entr_FinalPH').val());
  });
  $('#entr_InicioPH').change(() => {
    const varPH = $('#entr_InicioPH').val();
    obtenerFilaPH(varPH);
    calcularPH();
  });
  $('#perc_salfuman').change(() => {
    calcularPH();
  });
  $('#entr_cianurico').change(() => {
    calcularACc();
    calcularLSI();
  });
  $('#sliderAT').slider({
    slide: function() {
      const b = $('#sliderAT').slider('value');
      $('#entr_FinalAT').val(b);
      calcularACc();
      calcularTDS();
      calcularAT('ss');
    }
  });
  $('#sliderAT').slider({
    change: function() {
      const b = $('#sliderAT').slider('value');
      $('#entr_FinalAT').val(b);
      calcularACc();
      calcularTDS();
      calcularAT('sc');
    }
  });
  $('#entr_FinalAT').change(() => {
    const b = $('#entr_FinalAT').val();
    $('#sliderAT').slider('value', b);
  });
  $('#entr_InicioAT').change(() => {
    calcularAT('iis');
  });
  $('#sliderCL').slider({
    slide: function() {
      const b = $('#sliderCL').slider('value');
      $('#entr_FinalCL').val(b);
      calcularCL();
    }
  });
  $('#sliderCL').slider({
    change: function() {
      const b = $('#sliderCL').slider('value');
      $('#entr_FinalCL').val(b);
      calcularCL();
    }
  });
  $('#entr_FinalCL').change(() => {
    const b = $('#entr_FinalCL').val();
    $('#sliderCL').slider('value', b);
  });
  $('#entr_InicioCL').change(() => {
    calcularCL();
  });
  $('#perc_peroxido').change(() => {
    calcularCL();
  });
  $('#escala_CL').change(() => {
    let a;
    let b;
    if ($('#escala_CL').val() === '0 - 50 ppm') {
      b = $('#sliderCL').slider('value');
      $('#escalaCL').removeClass('fdo-escCL').addClass('fdo-escCL50');
      a = $('#sliderCL').slider();
      a.slider({
        min: 0
      });
      a.slider({
        max: 50
      });
      a.slider({
        step: 1
      });
      a.slider({
        animate: true
      });
    } else {
      b = $('#sliderCL').slider('value');
      $('#escalaCL').removeClass('fdo-escCL50').addClass('fdo-escCL');
      a = $('#sliderCL').slider();
      a.slider({
        min: 0
      });
      a.slider({
        max: 5
      });
      a.slider({
        step: 0.1
      });
      a.slider({
        animate: true
      });
      if (b > 5) {
        b = 5;
      }
    }
    a.slider('value', b);
  });
  $('#sliderBR').slider({
    slide: function() {
      const b = $('#sliderBR').slider('value');
      $('#entr_FinalBR').val(b);
      calcularBR();
    }
  });
  $('#sliderBR').slider({
    change: function() {
      const b = $('#sliderBR').slider('value');
      $('#entr_FinalBR').val(b);
      calcularBR();
    }
  });
  $('#entr_FinalBR').change(() => {
    const b = $('#entr_FinalBR').val();
    $('#sliderBR').slider('value', b);
  });
  $('#entr_InicioBR').change(() => {
    calcularBR();
  });
  $('#escala_BR').change(() => {
    let b = $('#sliderBR').slider('value');
    let a;
    if ($('#escala_BR').val() === '0 - 70 ppm') {
      $('#escalaBR').removeClass('fdo-escBR7').addClass('fdo-escBR70');
      a = $('#sliderBR').slider();
      a.slider({
        min: 0
      });
      a.slider({
        max: 70
      });
      a.slider({
        step: 1
      });
      a.slider({
        animate: true
      });
    } else {
      $('#escalaBR').removeClass('fdo-escBR70').addClass('fdo-escBR7');
      a = $('#sliderBR').slider();
      a.slider({
        min: 0
      });
      a.slider({
        max: 7
      });
      a.slider({
        step: 0.1
      });
      a.slider({
        animate: true
      });
      if (b > 7) {
        (b = 7);
      }
    }
    a.slider('value', b);
  });
  $('#sliderDC').slider({
    slide: function() {
      const b = $('#sliderDC').slider('value');
      $('#entr_FinalDC').val(b);
      calcularDC('ss');
      calcularTDS();
    }
  });
  $('#sliderDC').slider({
    change: function() {
      const b = $('#sliderDC').slider('value');
      $('#entr_FinalDC').val(b);
      calcularDC('sc');
      calcularTDS();
    }
  });
  $('#entr_FinalDC').change(() => {
    const b = $('#entr_FinalDC').val();
    $('#sliderDC').slider('value', b);
  });
  $('#entr_InicioDC').change(() => {
    calcularDC('iis');
  });
  $('#dur_calcica_agua_remplazo').change(() => {
    CambioDCAgRemp();
  });
  const a = {
    '0en': '0es',
    '1en': '1es',
    '2en': '2es',
    '3en': '3es',
    '4en': '4es',
    '5en': '5es',
    '6en': '6es',
    '7en': '7es',
    '8en': '8es',
    '9en': '9es',
    '10en': '10es',
    '11en': '11es',
    '12en': '12es',
    '13en': '13es',
    '14en': '14es',
    '15en': '15es',
    '16en': '16es',
    '17en': '17es',
    '18en': '18es',
    '19en': '19es',
    '20en': '20es',
    '21en': '21es',
    '22en': '22es',
    '23en': '23es',
    '24en': '24es',
    '25en': '25es',
    '26en': '26es',
    '27en': '27es',
    '28en': '28es',
    '29en': '29es',
    '30en': '30es',
    '31en': '31es',
    '32en': '32es',
    '33en': '33es',
    '34en': '34es',
    '35en': '35es',
    '36en': '36es',
    '37en': '37es',
    '38en': '38es',
    '39en': '39es',
    '39Aen': '39Aes',
    '40en': '40es',
    '41en': '41es',
    '42en': '42es',
    '43en': '43es',
    '44en': '44es',
    '45en': '45es',
    '46en': '46es',
    '47en': '47es',
    '48en': '48es',
    '49en': '49es',
    '49Aen': '49Aes',
    '50en': '50es',
    '51en': '51es',
    '52en': '52es',
    '53en': '53es',
    '54en': '54es',
    '55en': '55es',
    '56en': '56es',
    '57en': '57es',
    '58en': '58es',
    '59en': '59es',
    '59Aen': '59Aes',
    '59Ben': '59Bes',
    '60en': '60es',
    '61en': '61es',
    '62en': '62es',
    '63en': '63es',
    '63Ben': '63Bes',
    '64en': '64es',
    '65en': '65es',
    '66en': '66es',
    '67en': '67es',
    '68en': '68es',
    '69Aen': '69Aes',
    '69en': '69es',
    '69Ben': '69Bes',
    '70en': '70es',
    '71en': '71es',
    '72en': '72es',
    '73en': '73es',
    '74en': '74es',
    '75en': '75es',
    '76en': '76es',
    '77en': '77es',
    '78en': '78es',
    '79en': '79es',
    '80en': '80es',
    '81en': '81es',
    '82en': '82es',
    '83en': '83es',
    '84en': '84es',
    '85en': '85es',
    '86en': '86es',
    '87en': '87es',
    '90en': '90es',
    '91en': '91es',
    '92en': '92es',
    '93en': '93es',
    '94en': '94es',
    '95en': '95es'
  };
  const c = {
    TDS: '1',
    ACN: '1',
    PH: '1',
    AT: '1',
    DC: '1',
    CL: '1',
    BR: '1',
    CF: '1',
    VF: '1'
  };
  $('#v-ingles').click(() => {
    if ($('#miPiscina').text() !== 'MY POOL') {
      $.each(a, (b, a) => {
        b = '.' + b;
        a = '.' + a;

        $('#p1').load('idioma.html ' + b, function() {
          $(a).replaceWith($('#p1').html());
        });
      });
      $.each(c, (b) => {
        $('#ayuda' + b + '-aux').load('i18n/en-en/ayuda' + b + '.html');
      });
      $('#t-LSIes').addClass('ocultar');
      $('#t-LSIen').removeClass('ocultar');
      $('#imgPH').removeAttr('src').attr('src', 'i18n/en-en/images/ph.png');
      $('#imgAT').removeAttr('src').attr('src', 'i18n/en-en/images/ta.png');
      $('#imgDC').removeAttr('src').attr('src', 'i18n/en-en/images/ch.png');
    }
  });

  $('#v-espanol').click(() => {
    if ($('#miPiscina').text() !== 'MI PISCINA') {
      $.each(a, (b, a) => {
        b = '.' + b;
        a = '.' + a;

        $('#p1').load('idioma.html ' + a, function() {
          $(b).replaceWith($('#p1').html());
        });
      });
      $.each(c, (a) => {
        $('#ayuda' + a + '-aux').load('i18n/es-es/ayuda' + a + '.html');
      });
      $('#t-LSIen').addClass('ocultar');
      $('#t-LSIes').removeClass('ocultar');
      $('#imgPH').removeAttr('src').attr('src', 'i18n/es-es/images/ph.png');
      $('#imgAT').removeAttr('src').attr('src', 'i18n/es-es/images/ta.png');
      $('#imgDC').removeAttr('src').attr('src', 'i18n/es-es/images/ch.png');
    }
  });
});

